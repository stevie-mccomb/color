<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;

use App\Events\Color;

class HomeController extends Controller
{
    public function index()
    {
        $data['color'] = $this->getColor();

        return view('home', $data);
    }

    public function color()
    {
        $color = $this->generateColor();

        event(new Color($color));
        
        /*$options = array(
            'cluster' => 'us2',
            'encrypted' => true,
        );

        $pusher = new \Pusher\Pusher(
            'e97a19a0baae4d7e5d7e',
            'cf4098e3a1e82631811f',
            '515808',
            $options
        );

        $pusher->trigger('Color', 'Color', (Object) [
            'color' => $color,
        ]);*/
    }

    private function getColor()
    {
        $color = Session::get('color');

        if (empty($color)) $color = $this->generateColor();

        return $color;
    }

    private function generateColor()
    {
        $color = dechex(rand(0x000000, 0xFFFFFF));

        Session::put('color', $color);

        return $color;
    }
}
