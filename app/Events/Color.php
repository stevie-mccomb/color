<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class Color implements ShouldBroadcastNow
{
    use SerializesModels;

    public $color;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $color)
    {
        $this->color = $color;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('Color');
    }

    public function broadcastAs()
    {
        return 'Color';
    }
}
